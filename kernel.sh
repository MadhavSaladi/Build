#!/usr/bin/env bash
# Copyright (C) 2020 Saalim Quadri (iamsaalim)
# SPDX-License-Identifier: GPL-3.0-or-later

# Login to Git
echo -e "machine github.com\n  login $GITHUB_TOKEN" > ~/.netrc

# Set tg var.
function sendTG() {
    curl -s "https://api.telegram.org/bot$bot_token/sendmessage" --data "text=${*}&chat_id=-1001307329548&parse_mode=HTML" > /dev/null
}

# Setup arguments
PROJECT_DIR="$HOME"
KERNEL_DIR="$PROJECT_DIR/KharaMe"
TOOLCHAIN="$PROJECT_DIR/toolchain"
DEVICE="r5x"
BRANCH="r5x"
CHAT_ID="-1001307329548"

# Create kerneldir
mkdir -p "$PROJECT_DIR/KharaMe"

# Clone up the source
git clone https://github.com/MadhavSaladi/AnimeNames-Kernel -b $BRANCH $KERNEL_DIR --depth 1

# Clone toolchain
echo "Cloning toolchains"
git clone --depth=1 https://github.com/kdrag0n/proton-clang $TOOLCHAIN > /dev/null 2>&1

# Set Env
PATH="${TOOLCHAIN}/bin:${PATH}"
export ARCH=arm64
export KBUILD_BUILD_HOST=BabluS
export KBUILD_BUILD_USER="KharaMeCI"

# Build
cd "$KERNEL_DIR"
sendTG "Building KharaMe for r5x"

make O=out ARCH=arm64 vendor/r5x_defconfig
make -j24 O=out \ ARCH=arm64 \ CC=clang \ CROSS_COMPILE=aarch64-linux-gnu- \ CROSS_COMPILE_ARM32=arm-linux-gnueabi-


if [ ! -e  $KERNEL_DIR/out/arch/arm64/boot/Image.gz-dtb ]
then
    sendTG "Building KharaMe for r5x Failed"
    exit 1
fi

sendTG "Building KharaMe for r5x Completed. Uploading zip"


# Clone Anykernel
git clone https://gitlab.com/MadhavSaladi/AnyKernel3 -b r5x --depth 1 AnyKernel3
cp $KERNEL_DIR/out/arch/arm64/boot/Image.gz-dtb AnyKernel3/

now=`date +"%s"`

cd AnyKernel3 && zip -r9 KharaMe-r5x-"$now".zip * -x README.md KharaMe-r5x-"$now".zip

ZIP=$(echo *.zip)
curl -F chat_id="-1001307329548" -F document=@"$ZIP" "https://api.telegram.org/bot${bot_token}/sendDocument"
